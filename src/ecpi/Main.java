package ecpi;

public class Main {

	public static void main(String[] args) {
		if(args.length < 1){
			System.out.println("Usage: Toroidal Number Square");
			System.exit(1);
		}

		Toroidal s = new Toroidal();
		s.Square(Integer.parseInt(args[0]));
		s.compute();
		System.out.println("S = \n" + s);
		

	}

}
