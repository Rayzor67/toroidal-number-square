package ecpi;

public class Toroidal {
	
			protected int n;
			protected int[][] squareDimensions;
			protected int currentNumber;
			protected int currentRow;
			protected int currentCol;
			
			public void Square(int n){
				if(n % 2 == 0){
					System.out.println("Must enter an odd number");
				System.exit(1);
				}
				
				this.n = n;
				currentNumber = 1;
				squareDimensions = new int[n][n];
				currentRow = 0;
				currentCol = (n - 1) / 2;
			}
			
			public void compute(){
				squareDimensions[currentRow][currentCol] = currentNumber;
				currentNumber++; //currentNumber = currentNumber + 1
				
				int numberOfElements = n*n;
				while(currentNumber <= numberOfElements){
					int nextRow = getNextRow();
					int nextCol = getNextCol();
					
					if(squareDimensions[nextRow][nextCol] == 0){
						currentRow = nextRow;
						currentCol = nextCol;
						squareDimensions[currentRow][currentCol] = currentNumber;
					}
					else{//Cell is occupied
						currentRow = getNextValueOnSquare(currentRow + 1);
						squareDimensions[currentRow][currentCol] = currentNumber;
					}
					currentNumber++;
				}
			}
			
			private int getNextRow(){
				return getNextValueOnSquare(currentRow - 1);
			}
			
			private int getNextCol(){
				return getNextValueOnSquare(currentCol + 1);
			}
			
			private int getNextValueOnSquare(int value){
				int result = value % n;
				
				if(result >= 0){
					return result;
				}
				else{
					return n + result;
				}
			}
			
			public String toString(){
				StringBuilder stringBuilder = new StringBuilder();
				for(int row = 0; row < n; row++){
					for(int col = 0; col < n; col++){
						stringBuilder.append(squareDimensions[row][col] + "\t");
					}
					stringBuilder.append("\n");
				}
				return stringBuilder.toString();
			}


}


